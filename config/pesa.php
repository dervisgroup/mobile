<?php
return [
    'sandbox' => false,
    'c2b' => [
        'consumer_key' => 'lI1fGtJC9uShQgExKYDgUiGbmTRCTXbf',
        'consumer_secret' => '7Av1cg8RCeyAFLoF',
        'initiator' => 'APIUSER',
        'stk_callback' => env('APP_URL') . '/payments/callbacks/stk_callback',
        'id_validation_callback' => env('APP_URL') . '/payments/callbacks/validate',
        'callback_method' => 'POST',
        'short_code' => 917794,
        'passkey' => 'cee78f320351c06f16c1b8a4c0fa1b74791ff3b260ca089d9cf7a8a47aabe1ee',
        'security_credential' => 'DTD2YEiFKKr7RUwhfb85wSTUdUVgnmawsLp3XJgEggb9FFIPHYss7mClTGXOP4VG5QFCNSLlJ6rxoXo5RLM5TKLZKH9RWS3Sec17TAeYaIevezKzER9VAONBx9loP/uUze32SCJ8ukkrnHUWzqVyYh9FPqiMnRvosEN/V0ygkHWNMXn8h/iq2u8jKO4z4pT8IQtE1w1dZMazxlxW+PtJ2v9uKORvVSCctQ9iHfyfcNh3mHp6Uq6eO2yRzLtcs8C+XC/1lxAhmmqlCWf3lKTYcdKVOtxFqkIFTGbwOo06FcSc37wibeA1cXoZ6klwu8P8vS2+tbmBKpv6Dv/tdLO3MA==',
        'timeout_url' => env('APP_URL') . '/payments/callbacks/timeout',
        'result_url' => env('APP_URL') . '/payments/callbacks/result',
        'validation_url' => env('APP_URL') . '/payments/callbacks/validate',
        'confirmation_url' => env('APP_URL') . '/payments/callbacks/confirmation',
    ],
    'bulk' => [
        'consumer_key' => 'D1IAGmRnZxjhoHK2zdtrE4ny7IqpAuUA',
        'consumer_secret' => 'KSDrVAzKZJG7Hspu',
        'short_code' => 149396,
        'security_credential' => 'CLjvL+47Ocb0LTgY956szMiU6ls8whIQJXprduNtH6SBZp29/bz4d61gSA07IixluRhlTTRlQDl8Ihr8OL3+qhAj/lLPN7FLO8Ud+F5oxL4mI6hdDuhzcde+7ZFNkR5cGhRhnUfyVTdKpYblp2QX6UbLkdS5SJ45H0pYNkaMlXP3s6LUJSMydHNV6XzXhQCad3l2IoupyZr9lRkFplm4bkLdDSXRPq06XzZrgBAkpzlyccyz4KMbJKQnOokNl6C3E0LfOq1ZpjsGQgOhutAf3BFDYpSgoUpdeynv5vqTVuP7oBzg9PsR2fx+v58ZhtGrlZtw9NAWEs9VlFeGUBY1gA==',
        'initiator' => 'APIUSER',
        'timeout_url' => env('APP_URL') . '/payments/callbacks/timeout',
        'result_url' => env('APP_URL') . '/payments/callbacks/result',
    ],
    'notifications' => [
        'slack_web_hook' => 'https://hooks.slack.com/services/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',
        'only_important' => false,
    ],
];
